class Counter:
    def __init__(self, initial_value):
        self.value = initial_value

    def inc(self):
        self.value += 1
        return self.value

    def dec(self):
        self.value -= 1
        return self.value


class ReverseCounter(Counter):
    def inc(self):
        self.value -= 1
        return self.value

    def dec(self):
        self.value += 1
        return self.value


def get_counter(number):
    if number >= 0:
        return Counter(number)
    else:
        return ReverseCounter(number)


counter1 = get_counter(6)
print(counter1.inc())
print(counter1.dec())

counter2 = get_counter(-22)
print(counter2.inc())
print(counter2.dec())
