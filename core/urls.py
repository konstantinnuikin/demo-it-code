from django.urls import path

from core.views import *

urlpatterns = [
    path('', HomePage.as_view(), name='Home_Page'),
    path('category/<int:category_id>/', ThreadListView.as_view(), name='category_page'),
    path('thread/<int:thread_id>/', PostListView.as_view(), name='post_list'),
    path('user-posts/', UserPostsListView.as_view(), name='user_posts'),
    path('create/', CreatePostView.as_view(), name='create_post'),
    path('post_created/', PostCreatedView.as_view(), name='post_created'),
    path('user_threads/', UserThreadsView.as_view(), name='user_threads'),
    path('thread/<int:thread_id>/', ThreadDetailView.as_view(), name='thread_detail'),
    # path('register/', RegisterView.as_view(), name='register'),
    # path('login/', LoginView.as_view(), name='login'),
    # path('logout/', LogoutView.as_view(), name='logout'),
]