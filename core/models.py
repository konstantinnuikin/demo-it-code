from django.db import models
from django.shortcuts import render, redirect


from django.contrib.auth.models import AbstractUser


class Category(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Thread(models.Model):
    title = models.CharField(max_length=200)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    def __str__(self):
        return self.title


class Post(models.Model):
    thread = models.ForeignKey(Thread, on_delete=models.CASCADE)
    content = models.TextField()

    def __str__(self):
        return f"Post in {self.thread.title}"


class CustomUser(AbstractUser):
    user_name = models.CharField(max_length=100)
    threads = models.ManyToManyField('Thread', related_name='users', blank=True)
    posts = models.ManyToManyField('Post', related_name='users', blank=True)
