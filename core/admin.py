from django.contrib import admin

from core.models import *


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name',)


@admin.register(Thread)
class ThreadAdmin(admin.ModelAdmin):
    list_display = ('title',
                    'category',)


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ('thread',
                    'content',)


@admin.register(CustomUser)
class CustomUserAdmin(admin.ModelAdmin):
    list_display = ('user_name',)

    def threads(self, obj):
        return ", ".join([str(thread) for thread in obj.threads.all()])

    def posts(self, obj):
        return ", ".join([str(post) for post in obj.posts.all()])