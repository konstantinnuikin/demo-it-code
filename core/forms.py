from django import forms
from .models import Post, CustomUser, Category, Thread


class PostForm(forms.ModelForm):
    category = forms.ModelChoiceField(queryset=Category.objects.all())  # Added field for selecting category
    thread_title = forms.CharField()  # Added field for entering thread title
    coauthor = forms.ModelChoiceField(queryset=CustomUser.objects.all(), required=False)  # Added optional field for selecting coauthor

    class Meta:
        model = Post
        fields = ['category', 'thread_title', 'content', 'coauthor']  # Included category, thread_title, content, and coauthor fields

    def save(self, commit=True):
        post = super().save(commit=False)
        category = self.cleaned_data['category']
        thread_title = self.cleaned_data['thread_title']
        thread, _ = Thread.objects.get_or_create(title=thread_title, category=category)
        post.thread = thread
        if commit:
            post.save()
            self.save_m2m()
        return post

class RegistrationForm(forms.Form):
    username = forms.CharField(max_length=100)
    password = forms.CharField(widget=forms.PasswordInput)

class LoginForm(forms.Form):
    username = forms.CharField(max_length=100)
    password = forms.CharField(widget=forms.PasswordInput)