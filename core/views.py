from django.shortcuts import render, redirect
from django.views import View
from django.views.generic import TemplateView, ListView, CreateView
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.views import LoginView, LogoutView
from core.models import *
from core.forms import PostForm
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import RegistrationForm, LoginForm
import random


class HomePage(TemplateView):
    template_name = 'HTML_Core/HomePage.html'

    def get(self, request):
        context = self.get_context_data()
        return render(request, self.template_name, context)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        categories = Category.objects.order_by('?')[:6]
        context['categories'] = categories
        return context


class ThreadListView(ListView):
    model = Thread
    template_name = 'HTML_Core/thread_list.html'
    context_object_name = 'threads'

    def get_queryset(self):
        category_id = self.kwargs['category_id']
        category = Category.objects.get(id=category_id)
        return category.thread_set.all()


class PostListView(ListView):
    model = Post
    template_name = 'HTML_Core/post_list.html'
    context_object_name = 'posts'

    def get_queryset(self):
        thread_id = self.kwargs['thread_id']
        thread = Thread.objects.get(id=thread_id)
        return Post.objects.filter(thread=thread)


class UserPostsListView(ListView):
    model = Post
    template_name = 'HTML_Core/user_posts.html'
    context_object_name = 'posts'

    def get_queryset(self):
        user = self.request.user
        return Post.objects.filter(users=user)


class CreatePostView(View):
    def get(self, request):
        form = PostForm()
        return render(request, 'HTML_Core/create_post.html', {'form': form})

    def post(self, request):
        form = PostForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('post_created')
        return render(request, 'HTML_Core/create_post.html', {'form': form})


class PostCreatedView(View):
    def get(self, request):
        return render(request, 'HTML_Core/post_created.html')


# class UserThreadsView(ListView):
#     model = Thread
#     template_name = 'HTML_Core/user_threads.html'
#     context_object_name = 'threads'
#
#     def get_queryset(self):
#         user = self.request.user
#         return Thread.objects.filter(created_by=user)

class UserThreadsView(LoginRequiredMixin, View):
    def get(self, request):
        user = request.user
        threads = Thread.objects.filter(users=user)
        return render(request, 'HTML_Core/user_threads.html', {'threads': threads})

class ThreadDetailView(LoginRequiredMixin, View):
    def get(self, request, thread_id):
        thread = Thread.objects.get(id=thread_id)
        return render(request, 'HTML_Core/thread_detail.html', {'thread': thread})

# class RegisterView(View):
#     def get(self, request):
#         form = RegistrationForm()
#         return render(request, 'HTML_Core/register.html', {'form': form})
#
#     def post(self, request):
#         form = RegistrationForm(request.POST)
#         if form.is_valid():
#             username = form.cleaned_data['username']
#             password = form.cleaned_data['password']
#             user = CustomUser.objects.create_user(username=username, password=password)
#             return redirect('login')
#         return render(request, 'HTML_Core/register.html', {'form': form})
#
# class LoginView(View):
#     def get(self, request):
#         form = LoginForm()
#         return render(request, 'HTML_Core/login.html', {'form': form})
#
#     def post(self, request):
#         form = LoginForm(request.POST)
#         if form.is_valid():
#             username = form.cleaned_data['username']
#             password = form.cleaned_data['password']
#             try:
#                 user = CustomUser.objects.get(username=username)
#                 if user.check_password(password):
#                     request.session['user_id'] = user.id
#                     return redirect('Home_Page')
#                 else:
#                     form.add_error(None, 'Invalid username or password')
#             except CustomUser.DoesNotExist:
#                 form.add_error(None, 'Invalid username or password')
#         return render(request, 'HTML_Core/login.html', {'form': form})
#
# class LogoutView(View):
#     def get(self, request):
#         del request.session['user_id']
#         return redirect('Home_Page')