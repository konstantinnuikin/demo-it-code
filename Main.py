def day_month_year(day:int,month:int,year:int):
    """Функция вывода дд,мм,гг"""

    months = {
        1: 'января',
        2: 'февраля',
        3: 'марта',
        4: 'апреля',
        5: 'мая',
        6: 'июня',
        7: 'июля',
        8: 'августа',
        9: 'сентября',
        10: 'октября',
        11: 'ноября',
        12: 'декабря'
    }

    if month > 12 or month < 1:
        return 'Не верный формат'

    leap_year = False
    if year % 4 == 0:
        if year % 100 != 0 or year % 400 == 0:
            leap_year = True
    if month == 2 and leap_year:
        days_in_month = 29
    else:
        days_in_month = {
            1: 31,
            2: 28,
            3: 31,
            4: 30,
            5: 31,
            6: 30,
            7: 31,
            8: 31,
            9: 30,
            10: 31,
            11: 30,
            12: 31
        }[month]
    if day > days_in_month:
        return 'Неверная дата'

    return f'{day} {months[month]} {year} года'


print(day_month_year(32,13,1980))


def name(list_tuple:tuple):
    people = {}
    for i in list_tuple:
        if i in people:
            people[i] += 1
        else:
            people[i] = 1
    return people


print(name(('Олег', 'Игорь', 'Анна', 'Анна', 'Игорь', 'Анна', 'Василий')))


def persen_info(fml:dict):
    """Функция вывода пользователя из словаря"""

    if not fml :
        return 'Нет данных'

    first_name = fml.get('first_name', '')
    last_name = fml.get('last_name', '')
    middle_name = fml.get('middle_name', '')

    if not last_name:
        if first_name and middle_name:
            return f'{first_name} {middle_name}'
        elif middle_name:
            return 'Нет данных'
        else:
            return first_name

    if not first_name:
        if middle_name:
            return f'{last_name} {middle_name}'
        else:
            return last_name

    if middle_name:
        return f'{last_name} {first_name} {middle_name}'
    else:
        return f'{last_name} {first_name}'


fml = { 'middle_name': 'Иванов'}
print(persen_info(fml))


def is_prime(number:int) ->int:
    """Функция проверки простых чисел"""

    if not isinstance(number, int):
        raise TypeError("Аргумент должен быть целым числом")

    if number < 2:
        return False

    for i in range(2, int(number ** 0.5) + 1):
        if number % i == 0:
            return False

    return True


print(is_prime(22))


def unique_numbers(*args):
    """Функция создает список уникальных чисел из произвольных данных"""

    numbers = []
    for arg in args:
        if isinstance(arg, int):
            numbers.append(arg)

    unique_numbers = list(set(numbers))
    unique_numbers.sort()
    return unique_numbers


print(unique_numbers(1, '2', 'text', 42, None, None, None, 15, True, 1, 1, 15))
